<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PartyGagaModel extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	// public function __construct(){
	// 	parent::__construct();
	// 	$this->load->model('')
	// }

	public function index()
	{
		$data['style'] = $this->load->view('include/style', NULL, TRUE);
		$data['script'] = $this->load->view('include/script', NULL, TRUE);
		$data['footer'] = $this->load->view('template/footer', NULL, TRUE);
		$data['navbar'] = $this->load->view('template/header', NULL, TRUE);

		// $data['transaksi'] = $this->Part->get_movie();
		$this->load->view('page/home.php', $data);
	}

	public function transaksi(){
		$query = $this->db->query("SELECT * FROM transaksi");
		$result = $query->result_array();

		return $result;
	}
}